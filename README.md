# d-examples

d-examples contains code to demonstrate various things in D lang.

## Set up

After cloning or downloading, most programs should be a single file that can be compiled with ldc2 and run like any other executable, unless otherwise stated.

```
ldc2 -w -of'filename' 'filename.d' # compile file
./'filename' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
