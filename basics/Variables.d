import std.stdio;
import std.format;

int main(string[] args)
{
	// Integers
	int x = 4;
	int y = 2;
	writeln(format!"Integer Calculations, x = %s, y = %s"(x, y));

	writeln(format!"x + y = %s"(x + y)); // Addition
	writeln(format!"x - y = %s"(x - y)); // Subtraction
	writeln(format!"x * y = %s"(x * y)); // Multiplication
	writeln(format!"x / y = %s"(x / y)); // Division
	writeln(format!"x mod y = %s"(x % y)); // Modulus
	writeln(format!"x ^^ y = %s"(x ^^ y)); // Exponent

	// Floating point
	float a = 5.56f;
	float b = 8.00001;
	writeln(format!"\nFloating point calculations, a = %s, b = %s"(a, b));

	writeln(format!"a + b = %s"(a + b)); // Addition
	writeln(format!"a - b = %s"(a - b)); // Subtraction
	writeln(format!"a * b = %s"(a * b)); // Multiplication
	writeln(format!"a / b = %s"(a / b)); // Division
	writeln(format!"a mod b = %s"(a % b)); // Modulus
	writeln(format!"a ^^ b = %s"( a ^^ b)); // Exponent

	// Strings
	writeln("\nString Manipulation");

	string hello = "Hello";
	writeln(hello);

	hello = hello ~ " world ";
	writeln(hello);

	writeln(hello[2 .. 5]);

	// Arrays
	int[] numbers = new int[10];
	for(int i = 0; i < numbers.length; i++)
	{
		numbers[i] = i * 24;
	}

	writeln(numbers[3]);
	return 0;
}

