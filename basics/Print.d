import std.stdio;
import std.format;

int main(string[] args)
{
	// Hello World example
	writeln("Hello World!");

	// Printing variable example
	string text = "This is a test";
	writeln(text);

	// Printing multiple variables
	string name = "luke";
	int age = 37;
	writeln(format!"My name is %s and i am %s years old"(name, age));

	return 0;
}

