import std.stdio;
import std.format;

class Person 
{
private: 
	// Properties
	string _name;
	int _age;
public:
	// Constructor
	this(string name, int age)
	{
		setName(name);
		setAge(age);
	}

	// Getters
	string getName() 
	{
		return _name;
	}

	int getAge()
	{
		return _age;
	}

	// Setters
	void setName(string name)
	{
		_name = name;
	}

	void setAge(int age)
	{
		_age = age;
	}
}

int main(string[] args)
{
	Person p = new Person("luke", 101);

	// Calling getters
	writeln(format!"Name: %s Age: %s"(p.getName(), p.getAge()));

	return 0;
}

