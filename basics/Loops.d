import std.stdio;
import std.format;

int main(string[] args)
{
	bool running = true;
	int num = 0;

	// while loop
	while(running)
	{
		num += 1;
		if(num > 5)
		{
			running = false;
		}
	}

	// for loop
	for(int i = 1; i < 13; i++)
	{
		writeln(format!"512 + %s = %s"( i, 512 + i));
	}

	double[] numbers = [1.213, 2.454, 7.97305];

	foreach(double n ; numbers)
	{
		writeln(n);
	}

	return 0;
}

