import std.stdio;

// Function with no return statement or parameters
void sayHello()
{
	writeln("hello");
}

// Function that returns a string
string getWord()
{
	return "shoe";
}

// Function with single parameters
void doubleNumber(int x)
{
	writeln(x * 2);
}

// Function with multiple parameters and return statement
int addNumbers(int x, int y)
{
	return x + y;
}

int main(string[] args)
{
	// Calling Functions
	sayHello();
	writeln(getWord());
	doubleNumber(34);
	writeln(addNumbers(12,13));

	return 0;
}

