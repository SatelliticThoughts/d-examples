import std.stdio;

int main(string[] args)
{
	// Basic if statement.
	if(true)
	{
		writeln("This will show");
	}

	// Basic if/else statement.
	if(false)
	{
		writeln("This will NOT show");
	}
	else
	{
		writeln("This will show");
	}

	// Basic if/else if/else statement.
	if(false)
	{
		writeln("This will show");
	}
	else if(true)
	{
		writeln("This will show");
	}
	else
	{
		writeln("This will NOT show");
	}

	// &&, and
	if(true && true)
	{
		writeln("This will show");
	}

	if(true && false)
	{
		writeln("This will NOT show");
	}

	if(false && false)
	{
		writeln("This will NOT show");
	}

	// ||, or
	if(true || true)
	{
		writeln("This will show");
	}

	if(true || false)
	{
		writeln("This will show");
	}

	if(false || false)
	{
		writeln("This will NOT show");
	}

	int x = 90;
	int y = 90;

	//Equals
	if(x == y)
	{
		writeln("If x equals y this will show");
	}

	// Not Equal
	if(x != y)
	{
		writeln("If x does not equal y this will show");
	}

	// Greater Than
	if(x > y)
	{
		writeln("If x is bigger than y this will show");
	}

	// Less Than
	if(x < y)
	{
		writeln("If x is less than y this will show");
	}
	
	return 0;
}

