import std.conv;
import std.stdio;


void merge(int[] array, int left, int middle, int right)
{
	int leftLength = middle - left + 1;
	int rightLength = right - middle;

	int[] leftTemp = new int[leftLength];
	int[] rightTemp = new int[rightLength];

	for(int i = 0; i < leftLength; i++)
	{
		leftTemp[i] = array[left + i];
	}
	for(int i = 0; i < rightLength; i++)
	{
		rightTemp[i] = array[middle + 1 + i];
	}

	int i = 0;
	int j = 0;
	int k = left;

	while(i < leftLength && j < rightLength)
	{
		if(leftTemp[i] <= rightTemp[j])
		{
			array[k] = leftTemp[i];
			i++;
		}
		else
		{
			array[k] = rightTemp[j];
			j++;
		}
		k++;
	}

	while(i < leftLength)
	{
		array[k] = leftTemp[i];
		i++;
		k++;
	}

	while(j < rightLength)
	{
		array[k] = rightTemp[j];
		j++;
		k++;
	}
}


void mergeSort(int[] array, int left, int right)
{
	if(left < right)
	{
		int middle = (left + right) / 2;
		mergeSort(array, left, middle);
		mergeSort(array, middle + 1, right);
		
		merge(array, left, middle, right);
	}
}


void main(string[] args)
{
	writeln("Merge sort\n");
	
	int[] numbers = [2,6,8,4,2,1,3,5,7,8];
	
	writeln("Before sort");
	writeln(numbers);
	
	mergeSort(numbers, 0, to!int(numbers.length - 1));
	
	writeln("After sort");
	writeln(numbers);
}
