import std.stdio;


void bubbleSort(int[] array)
{
	for(int i = 0; i < array.length - 1; i++)
	{
		for(int j = 0; j < array.length - i - 1; j++)
		{
			if(array[j] > array[j + 1])
			{
				int temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
}


void main(string[] args)
{
	writeln("Bubble sort\n");
	
	int[] numbers = [2,6,3,1,3,0,9,73,4,5,7];
	writeln("Before sort");
	writeln(numbers);
	
	bubbleSort(numbers);
	
	writeln("After sort");
	writeln(numbers);
}
