import std.stdio;


void insertionSort(int [] array)
{
	for(int i = 0; i < array.length; i++)
	{
		int key = array[i];
		int j = i - 1;
		
		while(j >= 0 && array[j] > key)
		{
			array[j + 1] = array[j];
			j--;
		}
		array[j + 1] = key;
	}
}


void main(string[] args)
{
	writeln("Insertion sort\n");
	
	int[] numbers = [2,4,1,7,8,0,4,2,1];
	
	writeln("Before insertion");
	writeln(numbers);
	
	insertionSort(numbers);
	
	writeln("\nAfter sort");
	writeln(numbers);
}
