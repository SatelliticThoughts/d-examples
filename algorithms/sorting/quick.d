import std.conv;
import std.stdio;


int partition(int[] array, int low, int high)
{
	int pivot = array[high];
	int smallestIndex = low - 1;
	for(int i = low; i < high; i++)
	{
		if(array[i] < pivot)
		{
			smallestIndex++;
			int temp = array[smallestIndex];
			array[smallestIndex] = array[i];
			array[i] = temp;
		}
	}
	int temp = array[smallestIndex + 1];
	array[smallestIndex + 1] = array[high];
	array[high] = temp;
	
	return smallestIndex + 1;
}


void quickSort(int[] array, int low, int high)
{
	if(low < high)
	{
		int partitionIndex = partition(array, low, high);
		quickSort(array, low, partitionIndex - 1);
		quickSort(array, partitionIndex + 1, high); 
	}
}


void main(string[] args)
{
	writeln("Quick sort\n");
	
	int[] numbers = [2,4,6,2,1,3,5,7,9,0,8,5,7,3,5];
	writeln("Before sort");
	writeln(numbers);
	
	quickSort(numbers, 0, to!int(numbers.length - 1));
	
	writeln("\nAfter sort");
	writeln(numbers);
}
