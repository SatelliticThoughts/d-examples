import std.stdio;


void selectionSort(int[] array)
{
	for(int i = 0; i < array.length; i++)
	{
		int indexMin = i;
		
		for(int j = i + 1; j < array.length; j++)
		{
			if(array[j] < array[indexMin])
			{
				indexMin = j;
			}
		}
		int temp = array[i];
		array[i] = array[indexMin];
		array[indexMin] = temp;
	}
}


void main(string[] args)
{
	writeln("Selection sort\n");
	
	int[] numbers = [2,5,4,2,1,6,7,8,9,8,7];
	writeln("Before sort");
	writeln(numbers);
	
	selectionSort(numbers);
	
	writeln("\nAfter sort");
	writeln(numbers);
}
